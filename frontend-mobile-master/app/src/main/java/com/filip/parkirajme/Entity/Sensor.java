package com.filip.parkirajme.Entity;

import com.filip.parkirajme.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;


public class Sensor implements Device{
    private UUID id;
    private LatLng latLng;
    private boolean occupied;
    private UUID parkingId;
    private Marker marker;

    public Sensor(JSONObject jsonObject) {
        try {
            this.setId(UUID.fromString(jsonObject.getString("id")));
            this.setLatLng(new LatLng(jsonObject.getDouble("latitude"), jsonObject.getDouble("longitude")));
            this.setOccupied(jsonObject.getBoolean("occupied"));
            this.setParkingId(UUID.fromString(jsonObject.getString("parkingId")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public Sensor setOccupied(boolean occupied) {
        this.occupied = occupied;
        return this;
    }

    public UUID getParkingId() {
        return parkingId;
    }

    public void setParkingId(UUID parkingId) {
        this.parkingId = parkingId;
    }

    @Override
    public Marker getMarker() {
        return marker;
    }

    @Override
    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    @Override
    public int getDrawable() {
        if (this.isOccupied()) {
            return R.drawable.occupied_sensor;
        }
        else {
            return R.drawable.unoccupied_sensor;
        }
    }
}
