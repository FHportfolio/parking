package com.filip.parkirajme;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.filip.parkirajme.Entity.ParkingLot;
import com.filip.parkirajme.Entity.PaymentDevice;
import com.filip.parkirajme.Entity.Sensor;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;

public class InformationWindow implements GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    ConstraintLayout cl;

    private HashMap parkingLotHashMap;
    private final float TEXT_SIZE = 20;


    public InformationWindow(ConstraintLayout cl, HashMap parkingLotHashMap) {
        this.parkingLotHashMap = parkingLotHashMap;
        this.cl = cl;
        cl.setBackgroundColor(Color.WHITE);
        cl.setOnClickListener(v -> {
        });
        cl.setVisibility(View.INVISIBLE);
        cl.setBackgroundResource(R.drawable.round_edge_bg);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        cl.removeAllViews();
        if (marker.getTag() instanceof Sensor) {
            Sensor sensor = (Sensor) marker.getTag();
            Log.i("marker", "ovo je senzor");
            setInfoWindow((ParkingLot) parkingLotHashMap.get(sensor.getParkingId()), null);
        }
        if (marker.getTag() instanceof ParkingLot) {
            ParkingLot parkingLot = (ParkingLot) marker.getTag();
            Log.i("marker", "ovo je parking");
            setInfoWindow(parkingLot, null);
        }
        if (marker.getTag() instanceof PaymentDevice) {
            PaymentDevice paymentDevice = (PaymentDevice) marker.getTag();
            Log.i("marker", "ovo je placanje");
            setInfoWindow((ParkingLot) parkingLotHashMap.get(paymentDevice.getParkingId()), paymentDevice.getInfo());
        }
        cl.setVisibility(View.VISIBLE);
        return true;
    }

    public void setInfoWindow(ParkingLot parkingLot, String additionalInfo) {
        ConstraintSet set = new ConstraintSet();

        TextView parkingName = new TextView(cl.getContext());
        parkingName.setText(parkingLot.getParkingName());
        parkingName.setId(View.generateViewId());
        parkingName.setTextSize(TEXT_SIZE);
        cl.addView(parkingName);

        TextView zone = new TextView(cl.getContext());
        zone.setText(parkingLot.getZone().getName());
        zone.setId(View.generateViewId());
        zone.setTextSize(TEXT_SIZE);
        cl.addView(zone);

        TextView price = new TextView(cl.getContext());
        price.setText(String.format("%s %s", parkingLot.getZone().getCurrency(), parkingLot.getZone().getPrice()));
        price.setId(View.generateViewId());
        price.setTextSize(TEXT_SIZE);
        cl.addView(price);

        TextView occupied = new TextView(cl.getContext());
        occupied.setText(String.format("Parkings occupied: %s/%s", String.valueOf(parkingLot.getOccupiedSpaces()), String.valueOf(parkingLot.getMaxSpaces())));
        occupied.setId(View.generateViewId());
        occupied.setTextSize(TEXT_SIZE);
        cl.addView(occupied);

        if (additionalInfo != null) {
            TextView additionalInfoTextView = new TextView(cl.getContext());
            additionalInfoTextView.setText(additionalInfo);
            additionalInfoTextView.setId(View.generateViewId());
            additionalInfoTextView.setTextSize(TEXT_SIZE);
            cl.addView(additionalInfoTextView);
            set.clone(cl);
            set.connect(parkingName.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0);
            set.connect(zone.getId(), ConstraintSet.TOP, parkingName.getId(), ConstraintSet.BOTTOM, 40);
            set.connect(price.getId(), ConstraintSet.TOP, zone.getId(), ConstraintSet.BOTTOM, 40);
            set.connect(occupied.getId(), ConstraintSet.TOP, price.getId(), ConstraintSet.BOTTOM, 40);
            set.connect(additionalInfoTextView.getId(), ConstraintSet.TOP, occupied.getId(), ConstraintSet.BOTTOM, 40);
        } else {
            set.clone(cl);
            set.connect(parkingName.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0);
            set.connect(zone.getId(), ConstraintSet.TOP, parkingName.getId(), ConstraintSet.BOTTOM, 40);
            set.connect(price.getId(), ConstraintSet.TOP, zone.getId(), ConstraintSet.BOTTOM, 40);
            set.connect(occupied.getId(), ConstraintSet.TOP, price.getId(), ConstraintSet.BOTTOM, 40);
        }
        set.applyTo(cl);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        cl.setVisibility(View.INVISIBLE);
    }
}
