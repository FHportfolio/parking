package com.filip.parkirajme;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

public class FcmHandler {

    public void subscribeToTopic(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                .addOnFailureListener(f -> Log.d("FCM", "failed") )
                .addOnSuccessListener(f -> Log.d("FCM", "success"));
    }

    public void unsubscribeFromTopic(String topic) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
                .addOnSuccessListener(f -> Log.d("FCM" , "success"))
                .addOnFailureListener(f -> Log.d("FCM", "failed"));
    }
}
