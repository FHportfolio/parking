package com.filip.parkirajme.Entity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public interface Device {

    LatLng getLatLng();
    void setMarker(Marker marker);
    Marker getMarker();
    int getDrawable();
}
