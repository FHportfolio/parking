package com.filip.parkirajme;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.filip.parkirajme.Entity.Device;
import com.filip.parkirajme.Entity.ParkingLot;
import com.filip.parkirajme.Entity.PaymentDevice;
import com.filip.parkirajme.Entity.Sensor;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

/**
 * this class will take care of updating data on the map
 */

public class Parkings {

    private Context context;
    private FcmHandler fcmHandler;

    private HashMap<UUID, ParkingLot> parkingLotHashMap = new HashMap<>();
    private HashMap<UUID, Sensor> sensorHashMap = new HashMap<>();
    private HashMap<UUID, PaymentDevice> paymentDeviceHashMap = new HashMap<>();

    private double UPPER_LEFT_LAT = 100;
    private double UPPER_LEFT_LONG = 0;
    private double LOWER_RIGHT_LAT = 0;
    private double LOWER_RIGTH_LONG = 100;

    public Parkings (Context context) {
        this.context = context;
        fcmHandler = new FcmHandler();
        MapsActivity.googleMap.setOnCameraIdleListener(this::updateVisibility);
        getDevices();
    }

    private void getDevices() {

        RequestQueue queue = Volley.newRequestQueue(context);
        String url;
        url = "http://192.168.43.173:8070/public/parkingLots/?upperLeftLong=" + UPPER_LEFT_LONG + "&upperLeftLat="
                + UPPER_LEFT_LAT + "&lowerRightLong=" + LOWER_RIGTH_LONG + "&lowerRightLat=" + LOWER_RIGHT_LAT;
        JsonArrayRequest parkingLotRequest = new JsonArrayRequest(Request.Method.GET, url, null, response -> {
            for (int i = 0; i < response.length(); ++i) {
                ParkingLot parkingLot;
                try {
                    parkingLot = new ParkingLot(response.getJSONObject(i));
                    drawMarker(parkingLot);
                    fcmHandler.subscribeToTopic(parkingLot.getUuid().toString());
                    Log.i("FCM", "subscribed to " + parkingLot.getUuid().toString());
                    parkingLotHashMap.put(parkingLot.getUuid(), parkingLot);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            updateVisibility();
        }, error -> Log.e("response", error.toString()));

        url = "http://192.168.43.173:8070/public/parkingSensors/?upperLeftLong=" + UPPER_LEFT_LONG + "&upperLeftLat=" + UPPER_LEFT_LAT + "&lowerRightLong=" + LOWER_RIGTH_LONG + "&lowerRightLat=" + LOWER_RIGHT_LAT;

        JsonArrayRequest sensorRequest = new JsonArrayRequest(Request.Method.GET, url, null, response -> {
            for (int i = 0; i < response.length(); ++i) {
                try {
                    Sensor sensor = new Sensor(response.getJSONObject(i));
                    drawMarker(sensor);
                    sensorHashMap.put(sensor.getId(), sensor);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            updateVisibility();
        }, error -> Log.e("response", error.toString()));

        url = "http://192.168.43.173:8070/public/parkingDevices/?upperLeftLong=" + UPPER_LEFT_LONG + "&upperLeftLat=" + UPPER_LEFT_LAT + "&lowerRightLong=" + LOWER_RIGTH_LONG + "&lowerRightLat=" + LOWER_RIGHT_LAT;

        JsonArrayRequest paymentDeviceRequest = new JsonArrayRequest(Request.Method.GET, url, null, response -> {
            for (int i = 0; i < response.length(); ++i) {
                try {
                    PaymentDevice paymentDevice = new PaymentDevice(response.getJSONObject(i));
                    drawMarker(paymentDevice);
                    paymentDeviceHashMap.put(paymentDevice.getUuid(), paymentDevice);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            updateVisibility();
        }, error -> Log.e("response", error.toString()));

        queue.add(parkingLotRequest);
        queue.add(sensorRequest);
        queue.add(paymentDeviceRequest);
    }

    private static final String TAG = "FCM Service";

    public void onReceive(Context context, Intent intent) {
        UUID id = UUID.fromString(intent.getStringExtra("id"));
        Boolean occupied = intent.getStringExtra("occupied").equals("true");
        Sensor sensor = sensorHashMap.get(id);
        if(sensor.isOccupied() != occupied) {
            sensor.getMarker().remove();
            sensor.setOccupied(occupied);
            drawMarker(sensor);
            updateVisibility();
            sensorHashMap.put(sensor.getId(), sensor);
            ParkingLot parkingLot = parkingLotHashMap.get(sensor.getParkingId());
            if(!occupied) {
                parkingLot.setOccupiedSpaces(parkingLot.getOccupiedSpaces() - 1);
            }
            else {
                parkingLot.setOccupiedSpaces(parkingLot.getOccupiedSpaces() + 1);
            }
        }
        Log.d("FCM", id.toString());
        Log.d("FCM", occupied.toString());
    }

    private void updateVisibility() {
        if (MapsActivity.googleMap.getCameraPosition().zoom < 11.0) {
            this.hideParkings();
            this.hidePaymentDevices();
            this.hideSensors();
        }
        else if (MapsActivity.googleMap.getCameraPosition().zoom < 15.0) {
            this.showParkings();
            this.hidePaymentDevices();
            this.hideSensors();
        }
        else {
            this.hideParkings();
            this.showPaymentDevices();
            this.showSensors();
        }
    }

    private void showParkings() {
        for (UUID i : parkingLotHashMap.keySet()) {
            Objects.requireNonNull(parkingLotHashMap.get(i)).getMarker().setVisible(true);
        }
    }

    private void hideParkings() {
        for (UUID i : parkingLotHashMap.keySet()) {
            Objects.requireNonNull(parkingLotHashMap.get(i)).getMarker().setVisible(false);
        }
    }

    private void showSensors() {
        for (UUID i : sensorHashMap.keySet()) {
            Objects.requireNonNull(sensorHashMap.get(i)).getMarker().setVisible(true);
        }
    }

    private void hideSensors() {
        for (UUID i : sensorHashMap.keySet()) {
            Objects.requireNonNull(sensorHashMap.get(i)).getMarker().setVisible(false);
        }
    }

    private void showPaymentDevices() {
        for (UUID i : paymentDeviceHashMap.keySet()) {
            Objects.requireNonNull(paymentDeviceHashMap.get(i)).getMarker().setVisible(true);
        }
    }

    private void hidePaymentDevices() {
        for (UUID i : paymentDeviceHashMap.keySet()) {
            Objects.requireNonNull(paymentDeviceHashMap.get(i)).getMarker().setVisible(false);
        }
    }

    private void clearSensors() {
        for (UUID i : sensorHashMap.keySet()) {
            Objects.requireNonNull(sensorHashMap.get(i)).getMarker().remove();
        }
        sensorHashMap.clear();
    }

    private void clearParkings() {
        for (UUID i : parkingLotHashMap.keySet()) {
            Objects.requireNonNull(parkingLotHashMap.get(i)).getMarker().remove();
        }
        parkingLotHashMap.clear();
    }
    private void clearPaymentDevices() {
        for (UUID i : paymentDeviceHashMap.keySet()) {
            Objects.requireNonNull(paymentDeviceHashMap.get(i)).getMarker().remove();
        }
        paymentDeviceHashMap.clear();
    }

    private void drawMarker(Device device) {
        Drawable markerDrawable;
        markerDrawable = ContextCompat.getDrawable(context, device.getDrawable());
        assert markerDrawable != null;
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(markerDrawable);
        device.setMarker(MapsActivity.googleMap.addMarker(new MarkerOptions()
                .position(device.getLatLng())
                .icon(markerIcon).visible(false))
        );
        device.getMarker().setTag(device);
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public HashMap<UUID, ParkingLot> getParkingLotHashMap() {
        return parkingLotHashMap;
    }

    public HashMap<UUID, Sensor> getSensorHashMap() {
        return sensorHashMap;
    }

    public HashMap<UUID, PaymentDevice> getPaymentDeviceHashMap() {
        return paymentDeviceHashMap;
    }
}
