# kafka-simple-rest-proxy

Simple rest proxy for Kafka messaging service.

Installation:
*   go to support/docker/kafka
*   type docker-compose up in terminal
*   run application