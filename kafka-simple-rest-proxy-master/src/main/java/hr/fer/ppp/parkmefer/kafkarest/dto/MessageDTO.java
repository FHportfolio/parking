package hr.fer.ppp.parkmefer.kafkarest.dto;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

public class MessageDTO {

    @NotNull
    private UUID sensorId;
    @NotNull
    private Boolean occupied;
    private Date time;

    public MessageDTO() {
    }

    public UUID getSensorId() {
        return sensorId;
    }

    public void setSensorId(UUID sensorId) {
        this.sensorId = sensorId;
    }

    public Boolean getOccupied() {
        return occupied;
    }

    public void setOccupied(Boolean occupied) {
        this.occupied = occupied;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return String.format("sensorId: %s, occupied: %s, time: %s",sensorId, occupied, time);
    }
}
