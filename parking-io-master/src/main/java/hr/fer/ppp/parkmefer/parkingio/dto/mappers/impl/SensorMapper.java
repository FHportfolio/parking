package hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl;

import hr.fer.ppp.parkmefer.parkingio.dto.SensorDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Mapper;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Updater;
import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SensorMapper implements Mapper<Sensor, SensorDTO>, Updater<Sensor, SensorDTO> {

    @Override
    public Sensor dtoToEntity(SensorDTO dto) {
        return fillData(new Sensor(), dto);
    }

    @Override
    public SensorDTO entityToDto(Sensor entity) {
        SensorDTO sensorDTO = entityToDtoMinimal(entity);
        sensorDTO.setLastUpdated(entity.getLastUpdated());

        return sensorDTO;
    }

    @Override
    public SensorDTO entityToDtoMinimal(Sensor entity) {
        SensorDTO sensorDTO = new SensorDTO();

        sensorDTO.setId(entity.getId());
        sensorDTO.setLongitude(entity.getLongitude());
        sensorDTO.setLatitude(entity.getLatitude());
        sensorDTO.setOccupied(entity.getOccupied());
        sensorDTO.setParkingId(entity.getParkingId());

        return sensorDTO;
    }

    @Override
    public Sensor updateEntity(Sensor entity, SensorDTO dto) {
        return fillData(entity, dto);
    }

    private Sensor fillData(Sensor entity, SensorDTO dto) {
        entity.setLongitude(dto.getLongitude());
        entity.setLatitude(dto.getLatitude());
        entity.setLastUpdated(new Date());

        return entity;
    }
}
