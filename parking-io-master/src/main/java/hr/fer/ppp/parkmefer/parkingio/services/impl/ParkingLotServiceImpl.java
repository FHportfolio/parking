package hr.fer.ppp.parkmefer.parkingio.services.impl;

import hr.fer.ppp.parkmefer.parkingio.config.LogConfig;
import hr.fer.ppp.parkmefer.parkingio.dto.ParkingLotDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.PayingDeviceDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.SensorDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl.ParkingLotMapper;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl.PayingDeviceMapper;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl.SensorMapper;
import hr.fer.ppp.parkmefer.parkingio.entities.ParkingLot;
import hr.fer.ppp.parkmefer.parkingio.entities.PayingDevice;
import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import hr.fer.ppp.parkmefer.parkingio.entities.projections.ParkingLotProjection;
import hr.fer.ppp.parkmefer.parkingio.repositories.ParkingRepository;
import hr.fer.ppp.parkmefer.parkingio.services.ParkingLotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class ParkingLotServiceImpl extends AbstractService implements ParkingLotService {

    private static final Supplier<RuntimeException> ENTITY_NOT_FOUND = EntityNotFoundException::new;

    private final ParkingLotMapper parkingLotMapper;
    private final SensorMapper sensorMapper;
    private final PayingDeviceMapper payingDeviceMapper;
    private final ParkingRepository parkingRepository;

    @Autowired
    public ParkingLotServiceImpl(
            MessageSource messageSource,
            ParkingLotMapper parkingLotMapper,
            SensorMapper sensorMapper,
            PayingDeviceMapper payingDeviceMapper,
            ParkingRepository parkingRepository) {
        super(parkingRepository, messageSource);

        this.parkingLotMapper = parkingLotMapper;
        this.sensorMapper = sensorMapper;
        this.payingDeviceMapper = payingDeviceMapper;
        this.parkingRepository = parkingRepository;
    }

    @Override
    public List<ParkingLotDTO> getParkingLotsInRectangle(double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat) {
        List<ParkingLot> parkingLots = getParkingLotsInRectangleIntern(upperLeftLong, upperLeftLat, lowerRightLong, lowerRightLat);

        return parkingLots.stream().map(parkingLotMapper::entityToDtoMinimal).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ParkingLotDTO getParkingLotById(UUID id) {
        ParkingLot parkingLot = parkingRepository.getOne(id);

        return parkingLotMapper.entityToDto(parkingLot);
    }

    @Override
    public UUID registerParkingLot(ParkingLotDTO dto) {
        ParkingLot parkingLot = parkingLotMapper.dtoToEntity(dto);
        parkingLot = parkingRepository.save(parkingLot);

        if (dto.getSensors() != null && dto.getSensors().size() != 0) {
            for (SensorDTO sensor : dto.getSensors()) {
                Sensor entity = sensorMapper.dtoToEntity(sensor);
                entity.setParkingLot(parkingLot);
                parkingLot.getSensors().add(entity);
            }
        }

        if (dto.getPayingDevices() != null && dto.getPayingDevices().size() != 0) {
            for (PayingDeviceDTO payingDevice : dto.getPayingDevices()) {
                PayingDevice entity = payingDeviceMapper.dtoToEntity(payingDevice);
                entity.setParkingLot(parkingLot);
                parkingLot.getPayingDevices().add(entity);
            }
        }

        parkingRepository.save(parkingLot);
        log(LogConfig.PARKING_LOT_REGISTRATION, parkingLot.getId());

        return parkingLot.getId();
    }

    @Override
    public void editParkingLot(ParkingLotDTO parkingLotDTO) {
        ParkingLot parkingLot = parkingRepository.findById(parkingLotDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        parkingLotMapper.updateEntity(parkingLot, parkingLotDTO);

        parkingRepository.save(parkingLot);
        log(LogConfig.PARKING_LOT_EDIT, parkingLot.getId());
    }

    @Override
    public void deleteParkingLot(UUID id) {
        ParkingLot parkingLot = parkingRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);

        log(LogConfig.PARKING_LOT_DELETE, parkingLot.toString());
        parkingRepository.delete(parkingLot);
    }

    @Override
    public Page<ParkingLotProjection> getAllParkingLotsPageable(Pageable pageable) {
        return parkingRepository.findAllProjection(pageable);
    }

    @Override
    public void changeState(@NotNull UUID id, @NotNull Boolean enabled) {
        ParkingLot parkingLot = parkingRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);

        parkingLot.setEnabled(enabled);
        parkingRepository.save(parkingLot);
    }

    private List<ParkingLot> getParkingLotsInRectangleIntern(double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat) {
        return parkingRepository.findAllByLongitudeBetweenAndLatitudeBetweenAndEnabledIsTrue(upperLeftLong, lowerRightLong, lowerRightLat, upperLeftLat);
    }
}
