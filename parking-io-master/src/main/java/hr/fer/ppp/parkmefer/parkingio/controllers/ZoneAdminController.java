package hr.fer.ppp.parkmefer.parkingio.controllers;

import hr.fer.ppp.parkmefer.parkingio.dto.ResponseDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.ZoneDTO;
import hr.fer.ppp.parkmefer.parkingio.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "admin/zone")
public class ZoneAdminController extends AbstractController {

    private final ZoneService zoneService;

    @Autowired
    public ZoneAdminController(MessageSource messageSource, ZoneService zoneService) {
        super(messageSource);

        this.zoneService = zoneService;
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<ZoneDTO> getZoneById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(zoneService.getZoneById(id));
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerZone(@RequestBody @Valid ZoneDTO zoneDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        zoneService.registerZone(zoneDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editZone(@RequestBody @Valid ZoneDTO zoneDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        zoneService.editZone(zoneDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteZone(@RequestParam Long id) {
        zoneService.deleteZone(id);

        return ResponseEntity.ok(createResponseDTO());
    }

    @GetMapping(path = "/", produces = "application/json")
    public ResponseEntity<List<ZoneDTO>> getAllZones() {
        return ResponseEntity.ok(zoneService.getAllZones());
    }
}
