package hr.fer.ppp.parkmefer.parkingio.controllers;

import hr.fer.ppp.parkmefer.parkingio.config.MessageSourceConfig;
import hr.fer.ppp.parkmefer.parkingio.dto.ResponseDTO;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class AbstractController {

    private static final ResponseDTO VALID_RESPONSE = new ResponseDTO();
    private MessageSource messageSource;

    protected AbstractController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    protected ResponseDTO createResponseDTO(BindingResult bindingResult) {
        ResponseDTO responseDTO = new ResponseDTO();
        bindingResult.getAllErrors().forEach(error -> {
            FieldError fieldError = (FieldError) error;
            String fieldName = fieldError.getField();

            responseDTO.addError(fieldName, messageSource.getMessage(fieldError.getDefaultMessage(), null, MessageSourceConfig.LOCALES_MAP.get()));
        });

        return responseDTO;
    }

    protected ResponseDTO createResponseDTO() {
        return VALID_RESPONSE;
    }
}
