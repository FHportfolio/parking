package hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl;

import hr.fer.ppp.parkmefer.parkingio.common.money.MonetaryAmount;
import hr.fer.ppp.parkmefer.parkingio.dto.ZoneDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Mapper;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Updater;
import hr.fer.ppp.parkmefer.parkingio.entities.Zone;
import org.springframework.stereotype.Component;

import java.util.Currency;

@Component
public class ZoneMapper implements Mapper<Zone, ZoneDTO>, Updater<Zone, ZoneDTO> {

    private static final Currency HRK = Currency.getInstance("HRK");

    @Override
    public Zone dtoToEntity(ZoneDTO dto) {
        return new Zone(
                null,
                dto.getName(),
                dto.getPhone(),
                new MonetaryAmount(dto.getAmount(), HRK)
        );

    }

    @Override
    public ZoneDTO entityToDto(Zone entity) {
        ZoneDTO zoneDTO = new ZoneDTO();

        zoneDTO.setId(entity.getId());
        zoneDTO.setAmount(entity.getPrice().getAmount());
        zoneDTO.setCurrency(entity.getPrice().getCurrency().getCurrencyCode());
        zoneDTO.setName(entity.getName());
        zoneDTO.setPhone(entity.getPhone());

        return zoneDTO;
    }

    @Override
    public Zone updateEntity(Zone entity, ZoneDTO dto) {
        entity.setName(dto.getName());
        entity.setPrice(new MonetaryAmount(dto.getAmount(), Currency.getInstance(dto.getCurrency())));
        entity.setPhone(dto.getPhone());

        return entity;
    }
}
