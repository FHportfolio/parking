package hr.fer.ppp.parkmefer.parkingio.services;

import hr.fer.ppp.parkmefer.parkingio.dto.ParkingLotDTO;
import hr.fer.ppp.parkmefer.parkingio.entities.projections.ParkingLotProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface ParkingLotService {

    /**
     * @param upperLeftLong  longitude of upper left angle of the screen
     * @param upperLeftLat   latitude of upper left angle of the screen
     * @param lowerRightLong longitude of lower right angle of the screen
     * @param lowerRightLat  latitude of lower right angle of the screen
     * @return list of parking lots in desired rectangle
     */
    List<ParkingLotDTO> getParkingLotsInRectangle(
            double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat);

    /**
     * @param id parking lot id
     * @return parking lot with requested id
     * @throws javax.persistence.EntityNotFoundException
     */
    ParkingLotDTO getParkingLotById(@NotNull UUID id);

    /**
     * @param parkingLotDTO dto with data of parking lot to be registered
     * @return id of registered parking lot
     */
    UUID registerParkingLot(@NotNull ParkingLotDTO parkingLotDTO);

    /**
     * @param parkingLotDTO dto with new data of existing parking lot
     */
    void editParkingLot(@NotNull ParkingLotDTO parkingLotDTO);

    /**
     * @param id id of parking lot to be deleted
     */
    void deleteParkingLot(@NotNull UUID id);

    /**
     * @param pageable page request containing page number, page size and sort order
     * @return page containing list of {@link ParkingLotProjection}
     */
    Page<ParkingLotProjection> getAllParkingLotsPageable(@NotNull Pageable pageable);

    /**
     * @param id parking id
     * @param enabled state
     */
    void changeState(@NotNull UUID id, @NotNull Boolean enabled);
}