package hr.fer.ppp.parkmefer.parkingio.config;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.db.ColumnMapping;
import org.apache.logging.log4j.core.appender.db.jdbc.ColumnConfig;
import org.apache.logging.log4j.core.appender.db.jdbc.JdbcAppender;
import org.apache.logging.log4j.core.async.AsyncLoggerConfig;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Configuration
public class LogConfig {

    public static final String PARKING_LOT_REGISTRATION = "logger.parkingLot.register";
    public static final String PAYING_DEVICE_REGISTRATION = "logger.payingDevice.register";
    public static final String SENSOR_REGISTRATION = "logger.sensor.register";
    public static final String ZONE_REGISTRATION = "logger.zone.register";

    public static final String PARKING_LOT_EDIT = "logger.parkingLot.edit";
    public static final String PAYING_DEVICE_EDIT = "logger.payingDevice.edit";
    public static final String SENSOR_EDIT = "logger.sensor.edit";
    public static final String ZONE_EDIT = "logger.zone.edit";

    public static final String PARKING_LOT_DELETE = "logger.parkingLot.delete";
    public static final String PAYING_DEVICE_DELETE = "logger.payingDevice.delete";
    public static final String SENSOR_DELETE = "logger.sensor.delete";
    public static final String ZONE_DELETE = "logger.zone.delete";

    private final DataSource dataSource;

    @Autowired
    public LogConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    @PostConstruct
    public void onStartUp() {
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        org.apache.logging.log4j.core.config.Configuration config = ctx.getConfiguration();

        ColumnConfig[] columnConfigs = new ColumnConfig[]{
                ColumnConfig.newBuilder()
                        .setName("DATED")
                        .setPattern(null)
                        .setLiteral(null)
                        .setEventTimestamp(true)
                        .setUnicode(false)
                        .setClob(false)
                        .build(),
                ColumnConfig.newBuilder()
                        .setName("MESSAGE")
                        .setPattern("%message")
                        .setLiteral(null)
                        .setEventTimestamp(false)
                        .setUnicode(false)
                        .setClob(true)
                        .build(),
                ColumnConfig.newBuilder()
                        .setName("USER_ID")
                        .setPattern("%X{userId}")
                        .setLiteral(null)
                        .setEventTimestamp(false)
                        .setUnicode(false)
                        .setClob(false)
                        .build()};


        Appender jdbcAppender = JdbcAppender.newBuilder()
                .setBufferSize(0)
                .setColumnConfigs(columnConfigs)
                .setColumnMappings(new ColumnMapping[]{})
                .setConnectionSource(this::getConnection)
                .setTableName("LOGS")
                .withName("databaseAppender")
                .withIgnoreExceptions(true)
                .withFilter(null)
                .build();

        jdbcAppender.start();
        config.addAppender(jdbcAppender);

        AppenderRef ref = AppenderRef.createAppenderRef("JDBCAppender", null, null);
        AppenderRef[] refs = new AppenderRef[]{ref};

        LoggerConfig loggerConfig = AsyncLoggerConfig.createLogger(false, Level.INFO, "JDBCLogger", "true", refs, null, config, null);
        loggerConfig.addAppender(jdbcAppender, null, null);

        config.addLogger("JDBCLogger", loggerConfig);
        ctx.updateLoggers();
    }
}