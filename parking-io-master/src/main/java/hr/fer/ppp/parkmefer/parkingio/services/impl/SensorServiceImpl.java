package hr.fer.ppp.parkmefer.parkingio.services.impl;

import hr.fer.ppp.parkmefer.parkingio.config.LogConfig;
import hr.fer.ppp.parkmefer.parkingio.dto.SensorDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl.SensorMapper;
import hr.fer.ppp.parkmefer.parkingio.entities.ParkingLot;
import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import hr.fer.ppp.parkmefer.parkingio.repositories.ParkingRepository;
import hr.fer.ppp.parkmefer.parkingio.repositories.SensorRepository;
import hr.fer.ppp.parkmefer.parkingio.services.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class SensorServiceImpl extends AbstractService implements SensorService {

    private final SensorMapper sensorMapper;
    private final SensorRepository sensorRepository;

    @Autowired
    public SensorServiceImpl(MessageSource messageSource, ParkingRepository parkingRepository, SensorMapper sensorMapper, SensorRepository sensorRepository) {
        super(parkingRepository, messageSource);

        this.sensorMapper = sensorMapper;
        this.sensorRepository = sensorRepository;
    }

    @Override
    @Transactional
    public List<SensorDTO> getSensorsInRectangle(double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat) {
        List<SensorDTO> dtos = new ArrayList<>();

        List<Sensor> sensors = getSensorsInRectangleIntern(upperLeftLong, upperLeftLat, lowerRightLong, lowerRightLat);
        sensors.forEach(sensor -> dtos.add(sensorMapper.entityToDto(sensor)));

        return dtos;
    }

    @Override
    public SensorDTO getSensorById(UUID id) {
        Sensor sensor = sensorRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);

        return sensorMapper.entityToDto(sensor);
    }

    @Override
    @Transactional
    public UUID registerSensor(SensorDTO sensorDTO) {
        Sensor sensor = sensorMapper.dtoToEntity(sensorDTO);
        ParkingLot parkingLot = parkingRepository.getOne(sensorDTO.getParkingId());
        sensor.setParkingLot(parkingLot);

        sensorRepository.save(sensor);
        log(LogConfig.SENSOR_REGISTRATION, sensor.getId());

        return sensor.getId();
    }

    @Override
    public void editSensor(SensorDTO sensorDTO) {
        Sensor sensor = sensorRepository.findById(sensorDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        sensorMapper.updateEntity(sensor, sensorDTO);

        sensorRepository.save(sensor);
        log(LogConfig.SENSOR_EDIT, sensor.getId());
    }

    @Override
    public void deleteSensor(UUID id) {
        Sensor sensor = sensorRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);

        log(LogConfig.SENSOR_DELETE, sensor.toString());
        sensorRepository.delete(sensor);
    }

    private List<Sensor> getSensorsInRectangleIntern(double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat) {
        return sensorRepository.findAllByLongitudeBetweenAndLatitudeBetweenAndEnabledIsTrue(upperLeftLong, lowerRightLong, lowerRightLat, upperLeftLat);
    }
}
