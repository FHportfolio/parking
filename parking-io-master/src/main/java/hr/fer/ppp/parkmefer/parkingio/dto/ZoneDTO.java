package hr.fer.ppp.parkmefer.parkingio.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import hr.fer.ppp.parkmefer.parkingio.common.validator.money.Money;
import hr.fer.ppp.parkmefer.parkingio.common.validator.phone.Phone;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ZoneDTO implements Serializable {

    private static final long serialVersionUID = -5312995159808939340L;

    private Long id;

    @JsonProperty(required = true)
    @NotBlank(message = "appParkingForm.name.notBlank")
    @NotNull(message = "appParkingForm.name.notBlank")
    private String name;

    @JsonProperty(required = true)
    @NotBlank(message = "appParkingForm.name.notBlank")
    @NotNull(message = "appParkingForm.name.notBlank")
    @Phone(message = "appParkingForm.zone.phone.wrongFormat")
    private String phone;

    @JsonProperty(required = true)
    @NotNull(message = "appParkingForm.name.notBlank")
    @Money(message = "appParkingForm.amount.nonNegative")
    private BigDecimal amount;

    @JsonProperty(required = true)
    @NotNull(message = "appParkingForm.name.notBlank")
    private String currency;
}
