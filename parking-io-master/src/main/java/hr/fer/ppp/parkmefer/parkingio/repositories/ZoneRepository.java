package hr.fer.ppp.parkmefer.parkingio.repositories;

import hr.fer.ppp.parkmefer.parkingio.entities.Zone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZoneRepository extends JpaRepository<Zone, Long> {
}
