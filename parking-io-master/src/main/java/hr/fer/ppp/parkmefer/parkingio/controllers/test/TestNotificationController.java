package hr.fer.ppp.parkmefer.parkingio.controllers.test;

import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import hr.fer.ppp.parkmefer.parkingio.repositories.SensorRepository;
import hr.fer.ppp.parkmefer.parkingio.services.util.notifications.NotificationSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@RestController
@RequestMapping(path = "/notif")
@Profile("test")
public class TestNotificationController {

    private final NotificationSender notificationSender;
    private final SensorRepository sensorRepository;

    @Autowired
    public TestNotificationController(NotificationSender notificationSender, SensorRepository sensorRepository) {
        this.notificationSender = notificationSender;
        this.sensorRepository = sensorRepository;
    }

    @RequestMapping(path = "/")
    public ResponseEntity<String> sendNotification(@RequestParam("occupied") Boolean occupied) {
        Sensor sensor = sensorRepository.findById(UUID.fromString("0d1eb558-15a1-4a9b-9e7a-844457c204f3")).orElseThrow(EntityNotFoundException::new);
        sensor.setOccupied(occupied);
        notificationSender.sendNotification(sensor);

        return ResponseEntity.ok("ok");
    }
}
