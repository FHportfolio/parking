package hr.fer.ppp.parkmefer.parkingio.common.money;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Objects;

/**
 * A representation of money.
 * <p>
 * An amount object. Immutable.
 */
@Embeddable
public class MonetaryAmount implements Serializable {

    private static final long serialVersionUID = -3734467432803577280L;

    private BigDecimal amount;

    private Currency currency;

    /**
     * Create a new monetary amount from the specified amount.
     *
     * @param amount the amount of the amount; for example, in $USD "10.00" would be ten dollars, ".29" would be 29 cents
     */
    public MonetaryAmount(@NotNull BigDecimal amount, @NotNull Currency currency) {
        initValue(amount);
        this.currency = currency;
    }

    /**
     * Create a new monetary amount from the specified amount.
     *
     * @param amount the monetary amount as a double
     */
    public MonetaryAmount(double amount, @NotNull Currency currency) {
        initValue(BigDecimal.valueOf(amount));
        this.currency = currency;
    }

    @SuppressWarnings("unused")
    private MonetaryAmount() {
    }

    private void initValue(BigDecimal value) {
        this.amount = value.setScale(2, RoundingMode.HALF_EVEN);
    }

    /**
     * Convert the string representation of a monetary amount (e.g. $5 or 5) to a MonetaryAmount object.
     *
     * @param string the monetary amount string
     * @return the monetary amount object
     */
    public static MonetaryAmount valueOf(String string) {
        if (string == null || string.length() == 0) {
            throw new IllegalArgumentException("The monetary amount amount is required");
        }

        String[] parts = string.split(" ");
        if (parts.length != 2) {
            throw new IllegalArgumentException("Illegal format!");
        }

        BigDecimal value = new BigDecimal(parts[0]);
        Currency currency = Currency.getInstance(parts[1]);
        return new MonetaryAmount(value, currency);
    }

    /**
     * Add to this monetary amount, returning the sum as a new monetary amount.
     *
     * @param amount the amount to add
     * @return the sum
     */
    public MonetaryAmount add(MonetaryAmount amount) {
        if (amount.currency.equals(this.currency)) {
            return new MonetaryAmount(this.amount.add(amount.amount), currency);
        } else {
            throw new IllegalArgumentException("Currencies are not the same.");
        }
    }

    /**
     * Subtract from this monetary amount, returning the difference as a new monetary amount.
     *
     * @param amount the amount to subtract
     * @return the difference
     */
    public MonetaryAmount subtract(MonetaryAmount amount) {
        if (amount.currency.equals(this.currency)) {
            return new MonetaryAmount(this.amount.subtract(amount.amount), currency);
        } else {
            throw new IllegalArgumentException("Currencies are not the same.");
        }
    }

    /**
     * Multiply this monetary amount, returning the product as a new monetary amount.
     *
     * @param amount the amount to multiply
     * @return the product
     */
    public MonetaryAmount multiplyBy(BigDecimal amount) {
        return new MonetaryAmount(this.amount.multiply(amount), currency);
    }

    /**
     * Divide this monetary amount, returning the quotient as a decimal.
     *
     * @param amount the amount to divide by
     * @return the quotient
     */
    public BigDecimal divide(MonetaryAmount amount) {
        return this.amount.divide(amount.amount);
    }

    /**
     * Divide this monetary amount, returning the quotient as a new monetary amount.
     *
     * @param amount the amount to divide by
     * @return the quotient
     */
    public MonetaryAmount divideBy(BigDecimal amount) {
        return new MonetaryAmount(this.amount.divide(amount), currency);
    }

    /**
     * Returns true if this amount is greater than the amount.
     *
     * @param amount the monetary amount
     * @return true or false
     */
    public boolean greaterThan(MonetaryAmount amount) {
        return this.amount.compareTo(amount.amount) > 0;
    }

    /**
     * Get this amount as a double. Useful for when a double type is needed by an external API or system.
     *
     * @return this amount as a double
     */
    public double asDouble() {
        return amount.doubleValue();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public boolean equals(Object o) {
        if (!(o instanceof MonetaryAmount)) {
            return false;
        }

        MonetaryAmount other = (MonetaryAmount) o;
        return amount.equals(other.amount) && currency.equals(other.currency);
    }

    public int hashCode() {
        return Objects.hash(amount, currency);
    }

    public String toString() {
        return String.format("%s %s", amount, currency);
    }

}