package hr.fer.ppp.parkmefer.parkingio.security;

import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter used for avoiding cors origin bug while testing locally.
 */
@Configuration
public class CorsFilter implements Filter {

    private static final String ALLOWED_HEADERS = "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN";
    private static final String[] ACCESS_CONTROL_HEADERS = {
            "Access-Control-Allow-Origin",
            "Access-Control-Allow-Credentials",
            "Access-Control-Allow-Headers",
            "Access-Control-Expose-Headers",
            "Access-Control-Allow-Methods"
    };
    private static final String ALLOWED_METHODS = "GET, PUT, POST, DELETE, OPTIONS";
    private static final String ALLOWED_ORIGIN = "*";
    private static final String ACCESS_CONTROL_EXPOSE_HEADERS = "Content-Length, Content-Type, X-Requested-With";
    private static final String OPTIONS = "OPTIONS";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        response.setHeader(ACCESS_CONTROL_HEADERS[0], ALLOWED_ORIGIN);
        response.setHeader(ACCESS_CONTROL_HEADERS[1], Boolean.toString(true));
        response.setHeader(ACCESS_CONTROL_HEADERS[2], ALLOWED_HEADERS);
        response.setHeader(ACCESS_CONTROL_HEADERS[3], ACCESS_CONTROL_EXPOSE_HEADERS);
        response.setHeader(ACCESS_CONTROL_HEADERS[4], ALLOWED_METHODS);

        if (OPTIONS.equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(req, response);
        }
    }

    @Override
    public void destroy() {
    }
}
