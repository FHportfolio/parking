package hr.fer.ppp.parkmefer.parkingio.common.validator.phone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Phone, Object> {


    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (!(value instanceof String)) return false;

        String phone = (String) value;
        if (!(phone.startsWith("+") || phone.startsWith("00")) || phone.length() < 5) return false;

        if (phone.startsWith("+")) phone = phone.substring(1);

        return phone.chars().allMatch(Character::isDigit);
    }

    @Override
    public void initialize(Phone constraintAnnotation) {
    }
}
