import { Component, OnInit, Inject, Input } from '@angular/core';
import { Sensor } from '../dto/sensor.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ParkingService } from '../services/parking.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dialog-sensor',
  templateUrl: './dialog-sensor.component.html',
  styleUrls: ['./dialog-sensor.component.css']
})


export class DialogSensorComponent {

  @Input() parkingId: string;

  sensor: Sensor = new Sensor(null, null, null, null, null, null);

  

  constructor(public dialog: MatDialog, public parkingService: ParkingService) {
    console.log(this.parkingId);
    this.sensor = new Sensor(null, null, null, null, null, this.parkingId);
   }

  openDialog() {
    const dialogRef = this.dialog.open(DialogSensor, {
      width: '250px',
      data: { sensor: this.sensor }
    });

    dialogRef.afterClosed().subscribe(result => {
      /*       console.log(`Dialog result: ${result}`);
       */      /* this.parking = result; */
      if (! (this.sensor.latitude === null 
        || this.sensor.longitude === null) ) {
        console.log(this.sensor);
        this.sensor.parkingId = this.parkingId;
        this.parkingService.addSensor(this.sensor).subscribe(
          (response) => {
            console.log(response);
            this.parkingService.getsensorsFromBackend(this.parkingId).subscribe({
              next: (sensors) => {
                console.log(sensors.sensors);
                this.parkingService.setSensors(sensors.sensors);
              },
              error: (error) => console.log(error)
            });
          }
        );
      }
      else{
        console.log("Greska u dodavanju!");
      }
      this.sensor = new Sensor(null, null, null, null, null, this.parkingId);
    });
  }
}

@Component({
  selector: 'dialog-sensor',
  templateUrl: './dialog.sensor.html',
})
export class DialogSensor {
  public get data(): Sensor {
    return this._data;
  }
  public set data(value: Sensor) {
    this._data = value;
  }
  constructor(
    public dialogRef: MatDialogRef<DialogSensor>,
    @Inject(MAT_DIALOG_DATA)
    private _data: Sensor) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
