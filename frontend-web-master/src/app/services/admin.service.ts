import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest } from "@angular/common/http";

import { UsersHttp } from "../dto/pageble.model";
import { User } from "../dto/user.model";
import { Subject } from "rxjs";
import { Sensor } from "../dto/sensor.model";

@Injectable()
export class AdminService{

    private users: User[] = [];

    usersChanged = new Subject<User[]>();

    constructor(private httpClient: HttpClient){
        
    }

    getUsers() {
        return this.users.slice();
    }

    setUsers(users: User[]) {
        this.users = users;
        this.usersChanged.next(this.users.slice());
    }
    
    getUsersFromBackend() {
        return this.httpClient.get<UsersHttp>('/authorization/user/pageable?size=100')
    }

    sendInviteEmail(email: string, role: any) {
        const data = {
           "email": email,
           "roleId": role
        };
        
          const req = new HttpRequest('POST', '/authorization/user/invite', data , { reportProgress: true })
          return this.httpClient.request(req);
    }

    deleteUser(index: number, user: User) {
        this.users.splice(index, 1);
        this.usersChanged.next(this.users.slice());
        
        const req = new HttpRequest('DELETE', `/authorization/user/delete?id=${user.id}`, { reportProgress: true }); 
        return this.httpClient.request(req);
    }

}