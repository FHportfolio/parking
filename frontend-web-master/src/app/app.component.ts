import { Component, OnInit } from '@angular/core';
import { UserA } from './dto/user';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { Role } from './dto/role';
import { AuthorizationData } from './dto/authorization.http.model';

/* declare var ol: any; */

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  
  currentUser: AuthorizationData;

  title = 'park-me-fer';
  /* lat: 51.678418;
  lng: 7.809007; */

  /* latitude: number = 18.5204;
  longitude: number = 73.8567;

  /* ol: any; */
  /* map: any;  */

  constructor(
    private router: Router,
    private authenticationService: AuthService
) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
}

/*  
ngOnInit() {

  this.map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([15.972313, 45.800894]),
      zoom: 13
    })
  });

  this.map.on('click', function (args) {
    console.log(args.coordinate);
    var lonlat = ol.proj.transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
    console.log(lonlat);
    
    var lon = lonlat[0];
    var lat = lonlat[1];
    alert(`lat: ${lat} long: ${lon}`);
  });
}
  */


get isAdmin() {
    return (this.currentUser.user_data.roleId === Role.Admin); 
}

logout() {
  /* console.log('logout'); */
    this.authenticationService.logout();
    this.router.navigate(['/login']);
}
}

export function  getBaseUrl() {
  let backendHost;
  const hostname = window && window.location && window.location.hostname;

  if (hostname === 'real.host.com') {
    backendHost = 'real.host.com';
  } else if (hostname === 'localhost') {
    backendHost = 'http://localhost:8100';
  }
  return backendHost;
}
