import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent, getBaseUrl } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule } from '@angular/material';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ParkingDetailsComponent } from './parking-details/parking-details.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ModManageComponent } from './mod-manage/mod-manage.component';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import { AdminEditDashComponent } from './admin-edit-dash/admin-edit-dash.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSelectModule} from '@angular/material/select';



import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { DialogComponent, DialogTemplate} from './dialog/dialog.component';
import { ParkingService } from './services/parking.service';
import { SigninComponent } from './signin/signin.component';
import { ForgotPasswordComponent } from './signin/forgot-password.component';
import { AuthInterceptor } from './authorization/auth.interceptor';
import { UserService } from './services/user.service';
import { EditParkingZoneComponent } from './edit-parking-zone/edit-parking-zone.component';
import { ParkingZonesComponent } from './parking-zones/parking-zones.component';
import { ExistingZoneComponent } from './parking-zones/existing-zone.component';
import { AdminService } from './services/admin.service';
import { MapDialogComponent, DialogTemplateMap } from './map-dialog/map-dialog.component';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './login/login.component';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptop';
import { DialogSensorComponent, DialogSensor } from './dialog-sensor/dialog-sensor.component';
import { PayingDevicesComponent } from './paying-devices/paying-devices.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    ParkingDetailsComponent,
    EditProfileComponent,
    ModManageComponent,
    AdminEditDashComponent,
    DialogComponent,
    DialogTemplate,
    SigninComponent,
    ForgotPasswordComponent,
    EditParkingZoneComponent,
    ParkingZonesComponent,
    ExistingZoneComponent,
    MapDialogComponent,
    DialogTemplateMap,
    LoginComponent,
    DialogSensorComponent,
    DialogSensor,
    PayingDevicesComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatInputModule,
    MatFormFieldModule,
    CommonModule,
    FormsModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatSelectModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_KEY'
    })
  ],

  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ParkingService,
    UserService,
    AdminService,
    AuthService,
    {provide: 'BASE_URL', useFactory: getBaseUrl}, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent], 
  entryComponents: [DialogTemplate, DialogTemplateMap, DialogSensor]
})
export class AppModule { }
