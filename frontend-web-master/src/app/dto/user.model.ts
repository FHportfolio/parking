export class User{
    

    constructor(public ID: string,
        public email: string,
        public id: string,
        public name: string,
        public password: string,
        public phone: string,
        public roleId: number,
        public surname: string,){   
    }
}