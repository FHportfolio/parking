import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Parking } from '../dto/parking.model';

@Component({
  selector: 'app-map-dialog',
  templateUrl: './map-dialog.component.html',
  styleUrls: ['./map-dialog.component.css']
})
export class MapDialogComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogTemplateMap, {
      width: '250px',
      data: {/* parking: this.parking */}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      /* this.parking = result; */
     /*  console.log(this.parking);
      this.parkingService.saveParking(this.parking);
      this.parking = new Parking(null,null,null,null,null,null); */
    });
  }
}

@Component({
  selector: 'dialog-template',
  templateUrl: './dialogM.html',
  styleUrls: ['./dialogM.css']
})
export class DialogTemplateMap {
  lat: 51.678418;
  lng: 7.809007;
  constructor(
    public dialogRef: MatDialogRef<DialogTemplateMap>,
    @Inject(MAT_DIALOG_DATA) public data: Parking) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
