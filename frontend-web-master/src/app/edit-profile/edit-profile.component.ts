import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { User } from '../user';
import { FormGroup, FormControl, Validators } from '@angular/forms';

//import { Subscription } from "rxjs/";


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit, OnDestroy {

  /* trenutno se ne sačuvaju promjene pošto nigdje ne pohranjujemo*/
  
  dummyUser=new User(1, 'Savo', 'Savic', 'ss@gmail.com', '0987470629', 'test123', 2);
  userIndex: number; //za kasnije dohvaćanje iz polja usera
  profileForm: FormGroup;
  //private subscription: Subscription;

  onSubmit(){
    const newUser = this.profileForm.value;
    this.dummyUser.name=newUser.name;
    this.dummyUser.surname=newUser.surname;
    this.dummyUser.email=newUser.email;
    this.dummyUser.phone=newUser.phone;  
    
    this.navigateBack();

  }
  onCancel(){
    this.navigateBack();
  }

  private navigateBack() {
    this.router.navigate(['/']);
  }
  constructor(private router: Router, private route: ActivatedRoute) {}

  initForm(){
    let userName=this.dummyUser.name;
    let userSurname=this.dummyUser.surname;
    let userEmail=this.dummyUser.email;
    let userPhone=this.dummyUser.phone;
    let userPassword=this.dummyUser.password;


    this.profileForm=new FormGroup({
      'name': new FormControl(userName, Validators.required),
      'surname': new FormControl(userSurname, Validators.required),
      'email': new FormControl(userEmail, [Validators.required, Validators.email]),
      'phone': new FormControl(userPhone, Validators.required),
      'password': new FormControl(userPassword, Validators.required),

     });

     
   }

  ngOnInit() {
   /* this.subscription = this.route.params.subscribe(
      (params: any) => {
        if (params.hasOwnProperty('id')) {
          this.userIndex = +params['id'];
        }
      }
    );*/
    this.initForm();
  }
  ngOnDestroy() {
    //this.subscription.unsubscribe();
  }

}
