import { Component, OnInit, OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Parking } from '../dto/parking.model';
import { ParkingService } from '../services/parking.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { DialogTemplate } from '../dialog/dialog.component';

declare var ol: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  /* lat: 51.678418;
  lng: 7.809007; */
  /** Based on the screen size, switch from standard to one column per row */

  parkings: Parking[] = [];
  subscription: Subscription;

  latitude: number = 18.5204;
  longitude: number = 73.8567;

  /* ol: any; */
  map: any;


  /* brojac: number; */



  constructor(private breakpointObserver: BreakpointObserver, private parkingService: ParkingService, public dialog: MatDialog) {
  }

  size: number = 1;

  onToggle(i){
    this.parkingService.onOffParking(this.getParking(i)).subscribe(
      (response) => {
        console.log(response);
      }
    );
  }

  onClick(i: number) {
    /* console.log(this.parkings[i]); */
    const dialogRef = this.dialog.open(DialogTemplate, {
      width: '250px',
      data: { parking: this.getParking(i) }
    });

    dialogRef.afterClosed().subscribe(result => {
      /*       console.log(`Dialog result: ${result}`);
       */      /* this.parking = result; */
      if (!(result === undefined )) {
        console.log(result);
        this.parkingService.updateParking(i, result).subscribe(
          (response) => {
            console.log(response);
          }
        );
      }
      else{
        console.log("Greska pri dodavanju!");
      }
      /* this.parking = new Parking(null,null,null,null,null,null); */
    });
  }

  onDelete(i) {
    this.parkingService.deleteParking(i, this.parkings[i]).subscribe(
      (response) => {
        console.log(response);
      }
    );
  }


  /* cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      this.setParkings();
      if (matches) {
        this.parkings.forEach(element => {
          this.size = 3;
        });
        return this.parkings;
      }

      this.parkings.forEach(element => {
        this.size = 1;
      });
      return this.parkings;
    })
  ); */


  ngOnInit() {
    this.breakpointObserver
      .observe([Breakpoints.Handset])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.size = 3;
        }
        else {
          this.size = 1;
        }
      });

    this.subscription = this.parkingService.parkingsChanged
      .subscribe(
        (parkings: Parking[]) => {
          this.parkings = parkings;
        }
      );
    this.parkingService.getParkingsFromBackend().subscribe({
      next: (parking) => {
        console.log(parking.content);
        this.parkingService.setParkings(parking.content);
      },
      error: (error) => console.log(error)
    });

    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([15.972313, 45.800894]),
        zoom: 13
      })
    });
  
    this.map.on('click', function (args) {
      console.log(args.coordinate);
      var lonlat = ol.proj.transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
      console.log(lonlat);
      
      var lon = lonlat[0];
      var lat = lonlat[1];
      alert(`Latitude: ${lat} Longitude: ${lon}`);
    });

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getParking(i: number){
    return this.parkings.slice()[i];
  }

  /*  ngOnInit() {
     this.parkingService.getParkings().subscribe({
       next: (parking) => {
         console.log(parking.content);
         parking.content.forEach((parking, i = 0) => this.parkings[i++] = parking);
       },
       error: (error) => console.log(error)
     })
     this.getSensors();
      this.setParkings(this.parkingService.getParkings()); 
} */

  /* setParkings() {
    this.parkingService.fetchParkings().forEach((parking) => this.parkings.push(parking));
  } */

  /* putParking(parking: Parking) {
    this.parkings[DashboardComponent.brojac++] = parking;
  } */

 /*  getSensors() {
    this.parkingService.getSensorsData().subscribe({
      next: (sensor) => {
        console.log(sensor);
        this.parkingService.setSensorData(sensor);
      },
      error: (error) => console.log(error)
    })
  } */

  /* getUser(){
    this.parkingService.getUserData().subscribe({
      next: (user) => {
        console.log(user);
      },
      error: (error) => console.log(error)
    }

    )
  } */

}
