import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AdminService } from '../services/admin.service';

export interface Uloga {
    value: string;
    viewValue: string;
}

@Component({
    selector: 'app-mod-manage',
    templateUrl: './mod-manage.component.html',
    styleUrls: ['./mod-manage.component.css']
})
export class ModManageComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    selected = true;
    roles: Uloga[] = [
        { value: '1', viewValue: 'Admin' },
        { value: '2', viewValue: 'Moderator' }
    ];
    selectedValue: any = null;
    /* returnUrl: string; */
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private adminService: AdminService
    ) { }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;

        this.adminService.sendInviteEmail(this.f.username.value, this.selectedValue).subscribe(
            data => {
                this.loading = false;
                console.log(data);
            },
            error => {
                this.error = error;
                this.loading = false;
            }
        );
    }
    onChange(newValue) {
        console.log(newValue);
        this.selectedValue = newValue;
        this.selected = false;
    }
}
