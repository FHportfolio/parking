package hr.fer.ppp.parkmefer.authorization.repositories;

import hr.fer.ppp.parkmefer.authorization.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
