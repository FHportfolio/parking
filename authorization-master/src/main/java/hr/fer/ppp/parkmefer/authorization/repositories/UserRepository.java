package hr.fer.ppp.parkmefer.authorization.repositories;

import hr.fer.ppp.parkmefer.authorization.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByEmail(String email);
}
