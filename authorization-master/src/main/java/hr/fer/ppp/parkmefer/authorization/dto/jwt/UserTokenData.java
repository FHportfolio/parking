package hr.fer.ppp.parkmefer.authorization.dto.jwt;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class UserTokenData {

    private UUID id;
    private String email;
    private Integer roleId;
}
