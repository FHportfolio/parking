package hr.fer.ppp.parkmefer.authorization.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class ResponseDTO implements Serializable {

    private static final long serialVersionUID = 3995887124622774475L;

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    private Map<String, String> errors = new HashMap<>();

    public void addError(@NotNull String name, String message) {
        errors.put(name, message);
    }
}
