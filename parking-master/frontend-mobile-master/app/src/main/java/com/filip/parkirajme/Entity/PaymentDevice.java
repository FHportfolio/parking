package com.filip.parkirajme.Entity;

import com.filip.parkirajme.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class PaymentDevice implements Device {

    private UUID uuid;
    private LatLng latLng;
    private Marker marker;
    private String info;
    private UUID parkingId;

    public PaymentDevice(JSONObject jsonObject) {
        try {
            this.uuid = UUID.fromString(jsonObject.getString("id"));
            this.latLng = new LatLng(jsonObject.getDouble("latitude"), jsonObject.getDouble("longitude"));
            this.info = jsonObject.getString("info");
            this.parkingId = UUID.fromString(jsonObject.getString("parkingId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    @Override
    public Marker getMarker() {
        return marker;
    }

    @Override
    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public UUID getParkingId() {
        return parkingId;
    }

    public void setParkingId(UUID parkingId) {
        this.parkingId = parkingId;
    }

    @Override
    public int getDrawable() {
        return R.drawable.payment_device;
    }
}
