package hr.fer.ppp.parkmefer.parkingio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParkingIoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParkingIoApplication.class, args);
    }
}
