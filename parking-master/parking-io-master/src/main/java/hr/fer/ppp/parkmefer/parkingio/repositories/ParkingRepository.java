package hr.fer.ppp.parkmefer.parkingio.repositories;

import hr.fer.ppp.parkmefer.parkingio.entities.ParkingLot;
import hr.fer.ppp.parkmefer.parkingio.entities.projections.ParkingLotProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ParkingRepository extends JpaRepository<ParkingLot, UUID> {

    @Query("select lot from ParkingLot lot where lot.longitude between :minLong and :maxLong and lot.latitude between :minLat and :maxLat and enabled = true")
    List<ParkingLot> findAllByLongitudeBetweenAndLatitudeBetweenAndEnabledIsTrue(@Param("minLong") double minLongitude, @Param("maxLong") double maxLongitude, @Param("minLat") double minLatitude, @Param("maxLat") double maxLatitude);

    @Query("select p.id as id, p.longitude as longitude, p.latitude as latitude, p.name as name, p.enabled as enabled, p.maxSpaces as maxSpaces, p.occupiedSpaces as occupiedSpaces, p.zone.id as zoneId from ParkingLot p")
    Page<ParkingLotProjection> findAllProjection(Pageable pageable);
}
