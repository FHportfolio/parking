package hr.fer.ppp.parkmefer.parkingio.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@Table(name = "PARKING_LOTS")
public class ParkingLot implements Serializable {

    private static final long serialVersionUID = -7528540004808220355L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    @Column(name = "ID", updatable = false, nullable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private UUID id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "LONGITUDE", nullable = false)
    private Double longitude;

    @Column(name = "LATITUDE", nullable = false)
    private Double latitude;

    @Column(name = "MAX_SPACES", nullable = false)
    private Integer maxSpaces = 0;

    @Column(name = "OCCUPIED_SPACES", nullable = false)
    private Integer occupiedSpaces = 0;

    @Column(name = "ENABLED", nullable = false)
    private Boolean enabled;

    @ManyToOne
    @JoinColumn(name = "ZONE_ID")
    private Zone zone;

    @OneToMany(orphanRemoval = true, mappedBy = "parkingLot", cascade = CascadeType.ALL)
    private Set<Sensor> sensors;

    @OneToMany(orphanRemoval = true, mappedBy = "parkingLot", cascade = CascadeType.ALL)
    private Set<PayingDevice> payingDevices;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingLot that = (ParkingLot) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ParkingLot{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", longitude=").append(longitude);
        sb.append(", latitude=").append(latitude);
        sb.append(", maxSpaces=").append(maxSpaces);
        sb.append(", occupiedSpaces=").append(occupiedSpaces);
        sb.append(", enabled=").append(enabled);
        sb.append('}');
        return sb.toString();
    }
}
