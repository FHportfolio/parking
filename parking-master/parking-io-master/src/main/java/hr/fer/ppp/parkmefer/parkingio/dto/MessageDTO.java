package hr.fer.ppp.parkmefer.parkingio.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class MessageDTO {

    @NotNull
    private UUID sensorId;
    @NotNull
    private Boolean occupied;
    @NotNull
    private Date time;

    @Override
    public String toString() {
        return String.format("sensorId: %s, occupied: %s, time: %s", sensorId, occupied, time);
    }
}
