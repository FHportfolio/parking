package hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl;

import hr.fer.ppp.parkmefer.parkingio.dto.ParkingLotDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Mapper;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Updater;
import hr.fer.ppp.parkmefer.parkingio.entities.ParkingLot;
import hr.fer.ppp.parkmefer.parkingio.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.stream.Collectors;

@Component
public class ParkingLotMapper implements Mapper<ParkingLot, ParkingLotDTO>, Updater<ParkingLot, ParkingLotDTO> {

    private final ZoneRepository zoneRepository;
    private final PayingDeviceMapper payingDeviceMapper;
    private final SensorMapper sensorMapper;

    @Autowired
    public ParkingLotMapper(ZoneRepository zoneRepository, PayingDeviceMapper payingDeviceMapper, SensorMapper sensorMapper) {
        this.zoneRepository = zoneRepository;
        this.payingDeviceMapper = payingDeviceMapper;
        this.sensorMapper = sensorMapper;
    }

    @Override
    public ParkingLot dtoToEntity(ParkingLotDTO dto) {
        ParkingLot parkingLot = fillData(new ParkingLot(), dto);
        parkingLot.setPayingDevices(new HashSet<>());
        parkingLot.setSensors(new HashSet<>());

        if (dto.getSensors() != null) parkingLot.setMaxSpaces(dto.getSensors().size());

        return parkingLot;
    }

    @Override
    public ParkingLotDTO entityToDto(ParkingLot entity) {
        ParkingLotDTO parkingLotDTO = entityToDtoMinimal(entity);

        parkingLotDTO.setSensors(entity
                .getSensors()
                .stream()
                .map(sensorMapper::entityToDto)
                .collect(Collectors.toList())
        );
        parkingLotDTO.setPayingDevices(entity
                .getPayingDevices()
                .stream()
                .map(payingDeviceMapper::entityToDto)
                .collect(Collectors.toList())
        );

        return parkingLotDTO;
    }

    @Override
    public ParkingLotDTO entityToDtoMinimal(ParkingLot entity) {
        ParkingLotDTO parkingLotDTO = new ParkingLotDTO();

        parkingLotDTO.setName(entity.getName());
        parkingLotDTO.setId(entity.getId());
        parkingLotDTO.setLongitude(entity.getLongitude());
        parkingLotDTO.setLatitude(entity.getLatitude());
        parkingLotDTO.setMaxSpaces(entity.getMaxSpaces());
        parkingLotDTO.setOccupiedSpaces(entity.getOccupiedSpaces());
        parkingLotDTO.setEnabled(entity.getEnabled());
        parkingLotDTO.setZone(entity.getZone());

        return parkingLotDTO;

    }

    @Override
    public ParkingLot updateEntity(ParkingLot entity, ParkingLotDTO dto) {
        return fillData(entity, dto);
    }

    private ParkingLot fillData(ParkingLot entity, ParkingLotDTO dto) {
        entity.setName(dto.getName());
        entity.setEnabled(dto.getEnabled() != null ? dto.getEnabled() : false);
        entity.setLatitude(dto.getLatitude());
        entity.setLongitude(dto.getLongitude());
        entity.setZone(zoneRepository.findById(dto.getZoneId()).orElse(null));

        return entity;
    }
}
