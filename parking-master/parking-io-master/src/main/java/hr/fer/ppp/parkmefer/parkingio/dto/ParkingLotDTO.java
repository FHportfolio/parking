package hr.fer.ppp.parkmefer.parkingio.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import hr.fer.ppp.parkmefer.parkingio.entities.Zone;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
public class ParkingLotDTO implements Serializable {

    private static final long serialVersionUID = -845616405227586219L;

    private UUID id;

    @JsonProperty(required = true)
    @NotBlank(message = "appParkingForm.name.notBlank")
    @NotNull(message = "appParkingForm.name.notBlank")
    private String name;

    @JsonProperty(required = true)
    @Min(value = -180, message = "appParkingForm.longitude.min")
    @Max(value = 180, message = "appParkingForm.longitude.max")
    @NotNull(message = "appParkingForm.name.notBlank")
    private Double longitude;

    @JsonProperty(required = true)
    @Min(value = -90, message = "appParkingForm.latitude.min")
    @Max(value = 90, message = "appParkingForm.latitude.max")
    @NotNull(message = "appParkingForm.name.notBlank")
    private Double latitude;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer maxSpaces;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer occupiedSpaces;

    @JsonProperty(defaultValue = "false")
    private Boolean enabled;

    @JsonProperty(required = true)
    @NotNull(message = "appParkingForm.name.notBlank")
    private Long zoneId;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Zone zone;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<SensorDTO> sensors;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<PayingDeviceDTO> payingDevices;
}
