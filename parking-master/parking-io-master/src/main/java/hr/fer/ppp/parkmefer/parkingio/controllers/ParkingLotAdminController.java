package hr.fer.ppp.parkmefer.parkingio.controllers;

import hr.fer.ppp.parkmefer.parkingio.dto.ParkingLotDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.ResponseDTO;
import hr.fer.ppp.parkmefer.parkingio.entities.projections.ParkingLotProjection;
import hr.fer.ppp.parkmefer.parkingio.services.ParkingLotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(path = "admin/parkingLot")
public class ParkingLotAdminController extends AbstractController {

    private ParkingLotService parkingLotService;

    @Autowired
    public ParkingLotAdminController(MessageSource messageSource, ParkingLotService parkingLotService) {
        super(messageSource);

        this.parkingLotService = parkingLotService;
    }

    @GetMapping(path = "/pageable", produces = "application/json")
    public ResponseEntity<Page<ParkingLotProjection>> getParkingLotsPageable(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
        return ResponseEntity.ok(parkingLotService.getAllParkingLotsPageable(PageRequest.of(page, size)));
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<ParkingLotDTO> getParkingById(@PathVariable(name = "id") UUID id) {
        return ResponseEntity.ok(parkingLotService.getParkingLotById(id));
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerParkingLot(@RequestBody @Valid ParkingLotDTO parkingLotDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        parkingLotService.registerParkingLot(parkingLotDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editParkingLot(@RequestBody @Valid ParkingLotDTO parkingLotDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        parkingLotService.editParkingLot(parkingLotDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteParkingLot(@RequestParam UUID id) {
        parkingLotService.deleteParkingLot(id);

        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/changeState", produces = "application/json")
    public ResponseEntity<ResponseDTO> enableParkingLot(@RequestParam UUID id, @RequestParam Boolean enabled) {
        parkingLotService.changeState(id, enabled);

        return ResponseEntity.ok(createResponseDTO());
    }
}
