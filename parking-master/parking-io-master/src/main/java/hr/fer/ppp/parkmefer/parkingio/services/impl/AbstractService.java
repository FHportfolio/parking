package hr.fer.ppp.parkmefer.parkingio.services.impl;

import hr.fer.ppp.parkmefer.parkingio.config.MessageSourceConfig;
import hr.fer.ppp.parkmefer.parkingio.repositories.ParkingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

public abstract class AbstractService {

    protected final ParkingRepository parkingRepository;
    protected static final Supplier<RuntimeException> ENTITY_NOT_FOUND = EntityNotFoundException::new;
    private final MessageSource messageSource;

    private final Logger JDBC_LOGGER = LoggerFactory.getLogger("JDBCLogger");

    protected AbstractService(ParkingRepository parkingRepository, MessageSource messageSource) {
        this.parkingRepository = parkingRepository;
        this.messageSource = messageSource;
    }

    protected void log(String code, Object... args) {
        JDBC_LOGGER.info(
                messageSource.getMessage(
                        code,
                        args,
                        MessageSourceConfig.LOCALES_MAP.get()
                ));
    }
}
