package hr.fer.ppp.parkmefer.parkingio.services.impl;

import hr.fer.ppp.parkmefer.parkingio.config.LogConfig;
import hr.fer.ppp.parkmefer.parkingio.dto.PayingDeviceDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl.PayingDeviceMapper;
import hr.fer.ppp.parkmefer.parkingio.entities.ParkingLot;
import hr.fer.ppp.parkmefer.parkingio.entities.PayingDevice;
import hr.fer.ppp.parkmefer.parkingio.repositories.ParkingRepository;
import hr.fer.ppp.parkmefer.parkingio.repositories.PayingDeviceRepository;
import hr.fer.ppp.parkmefer.parkingio.services.PayingDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PayingDeviceServiceImpl extends AbstractService implements PayingDeviceService {

    private final PayingDeviceMapper payingDeviceMapper;
    private final PayingDeviceRepository payingDeviceRepository;

    @Autowired
    public PayingDeviceServiceImpl(MessageSource messageSource, ParkingRepository parkingRepository, PayingDeviceMapper payingDeviceMapper, PayingDeviceRepository payingDeviceRepository) {
        super(parkingRepository, messageSource);

        this.payingDeviceRepository = payingDeviceRepository;
        this.payingDeviceMapper = payingDeviceMapper;
    }

    @Override
    public List<PayingDeviceDTO> getPayingDevicesInRectangle(double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat) {
        List<PayingDeviceDTO> dtos = new ArrayList<>();

        List<PayingDevice> payingDevices = getPayingDevicesInRectangleIntern(upperLeftLong, upperLeftLat, lowerRightLong, lowerRightLat);
        payingDevices.forEach(payingDevice -> dtos.add(payingDeviceMapper.entityToDto(payingDevice)));

        return dtos;
    }

    @Override
    public PayingDeviceDTO getPayingDeviceById(UUID id) {
        PayingDevice payingDevice = payingDeviceRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);

        return payingDeviceMapper.entityToDto(payingDevice);
    }

    @Override
    @Transactional
    public UUID registerPayingDevice(PayingDeviceDTO payingDeviceDTO) {
        PayingDevice payingDevice = payingDeviceMapper.dtoToEntity(payingDeviceDTO);
        ParkingLot parkingLot = parkingRepository.getOne(payingDeviceDTO.getParkingId());
        payingDevice.setParkingLot(parkingLot);

        payingDeviceRepository.save(payingDevice);
        log(LogConfig.PAYING_DEVICE_REGISTRATION, payingDevice.getId());

        return payingDevice.getId();
    }

    @Override
    public void editPayingDevice(PayingDeviceDTO payingDeviceDTO) {
        PayingDevice payingDevice = payingDeviceRepository.findById(payingDeviceDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        payingDeviceMapper.updateEntity(payingDevice, payingDeviceDTO);

        payingDeviceRepository.save(payingDevice);
        log(LogConfig.PAYING_DEVICE_EDIT, payingDevice.getId());
    }

    @Override
    public void deletePayingDevice(UUID id) {
        PayingDevice payingDevice = payingDeviceRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);

        log(LogConfig.PAYING_DEVICE_DELETE, payingDevice.toString());
        payingDeviceRepository.delete(payingDevice);
    }

    private List<PayingDevice> getPayingDevicesInRectangleIntern(double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat) {
        return payingDeviceRepository.findAllByLongitudeBetweenAndLatitudeBetweenAndEnabledIsTrue(upperLeftLong, lowerRightLong, lowerRightLat, upperLeftLat);
    }
}
