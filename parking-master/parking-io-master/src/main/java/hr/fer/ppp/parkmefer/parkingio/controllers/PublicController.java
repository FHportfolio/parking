package hr.fer.ppp.parkmefer.parkingio.controllers;

import hr.fer.ppp.parkmefer.parkingio.dto.ParkingLotDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.PayingDeviceDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.SensorDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.ZoneDTO;
import hr.fer.ppp.parkmefer.parkingio.services.ParkingLotService;
import hr.fer.ppp.parkmefer.parkingio.services.PayingDeviceService;
import hr.fer.ppp.parkmefer.parkingio.services.SensorService;
import hr.fer.ppp.parkmefer.parkingio.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "public")
public class PublicController {

    private final ParkingLotService parkingLotService;
    private final SensorService sensorService;
    private final PayingDeviceService payingDeviceService;
    private final ZoneService zoneService;

    @Autowired
    public PublicController(ParkingLotService parkingLotService, SensorService sensorService, PayingDeviceService payingDeviceService, ZoneService zoneService) {
        this.parkingLotService = parkingLotService;
        this.sensorService = sensorService;
        this.payingDeviceService = payingDeviceService;
        this.zoneService = zoneService;
    }

    @GetMapping(path = "/parkingLots", produces = "application/json")
    public ResponseEntity<List<ParkingLotDTO>> getParkingLots(
            @RequestParam Double upperLeftLong,
            @RequestParam Double upperLeftLat,
            @RequestParam Double lowerRightLong,
            @RequestParam Double lowerRightLat) {

        return ResponseEntity.ok(parkingLotService.getParkingLotsInRectangle(upperLeftLong, upperLeftLat, lowerRightLong, lowerRightLat));
    }

    @GetMapping(path = "/parkingSensors", produces = "application/json")
    public ResponseEntity<List<SensorDTO>> getParkingSensors(
            @RequestParam Double upperLeftLong,
            @RequestParam Double upperLeftLat,
            @RequestParam Double lowerRightLong,
            @RequestParam Double lowerRightLat) {

        return ResponseEntity.ok(sensorService.getSensorsInRectangle(upperLeftLong, upperLeftLat, lowerRightLong, lowerRightLat));
    }

    @GetMapping(path = "/parkingDevices", produces = "application/json")
    public ResponseEntity<List<PayingDeviceDTO>> getPayingDevices(
            @RequestParam Double upperLeftLong,
            @RequestParam Double upperLeftLat,
            @RequestParam Double lowerRightLong,
            @RequestParam Double lowerRightLat) {

        return ResponseEntity.ok(payingDeviceService.getPayingDevicesInRectangle(upperLeftLong, upperLeftLat, lowerRightLong, lowerRightLat));
    }

    @GetMapping(path = "/zones", produces = "application/json")
    public ResponseEntity<List<ZoneDTO>> getAllZones() {
        return ResponseEntity.ok(zoneService.getAllZones());
    }
}
