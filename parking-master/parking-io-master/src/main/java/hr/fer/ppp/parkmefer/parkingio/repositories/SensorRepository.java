package hr.fer.ppp.parkmefer.parkingio.repositories;

import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface SensorRepository extends JpaRepository<Sensor, UUID> {
    @Query("select sens from Sensor sens join sens.parkingLot as lot where lot.longitude between :minLong and :maxLong and lot.latitude between :minLat and :maxLat and lot.enabled = true")
    List<Sensor> findAllByLongitudeBetweenAndLatitudeBetweenAndEnabledIsTrue(@Param("minLong") double minLongitude, @Param("maxLong") double maxLongitude, @Param("minLat") double minLatitude, @Param("maxLat") double maxLatitude);

}
