package hr.fer.ppp.parkmefer.parkingio.dto.mappers;

/**
 * Interface which defines a class which can transform dto to dto and vice versa.
 *
 * @param <E> dto (entity) type
 * @param <D> dto type
 */
public interface Mapper<E, D> {

    /**
     * Maps information from dto to corresponded entity which can then be persisted or furtherly processed.
     *
     * @param dto data transfer object
     * @return new entity with information mapped from dto
     */
    E dtoToEntity(D dto);

    /**
     * Maps information from entity to corresponded dto which can then be transferred via network.
     *
     * @param entity orm entity
     * @return data transfer object
     */
    D entityToDto(E entity);

    /**
     * Maps minimal information from entity to corresponded dto.
     *
     * @param entity orm entity
     * @return data transfer object
     * @throws UnsupportedOperationException if method is not supported by the implementation
     */
    default D entityToDtoMinimal(E entity) {
        throw new UnsupportedOperationException();
    }
}
