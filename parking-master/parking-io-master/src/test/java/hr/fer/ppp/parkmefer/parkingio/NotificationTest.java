package hr.fer.ppp.parkmefer.parkingio;

import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import hr.fer.ppp.parkmefer.parkingio.repositories.SensorRepository;
import hr.fer.ppp.parkmefer.parkingio.services.util.notifications.NotificationSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class NotificationTest {

    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private NotificationSender notificationSender;

    public NotificationTest() {
    }

    @Test
    public void test() {
        UUID id = UUID.fromString("0d1eb558-15a1-4a9b-9e7a-844457c204f3");

        Sensor sensor = sensorRepository.findById(id).get();
        notificationSender.sendNotification(sensor);
    }
}
