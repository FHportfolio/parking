import {
  Injectable, Inject,
} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {
  Observable
} from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private readonly baseUrl: string;

  constructor(@Inject('BASE_URL') url: string) {
    this.baseUrl = url;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const apiReq = req.clone({ url: `${this.baseUrl}${req.url}`});
    return next.handle(apiReq);
  }
}
