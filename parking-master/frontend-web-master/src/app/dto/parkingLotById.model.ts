import { Sensor } from "./sensor.model";

export interface PayingDevice {
    id: string;
    info: string;
    latitude: number;
    longitude: number;
    parkingId: string;
}

export interface Price {
    amount: number;
    currency: string;
}

export interface Zone {
    id: number;
    name: string;
    phone: string;
    price: Price;
}

export interface ParkingLotAll {
    enabled: boolean;
    id: string;
    latitude: number;
    longitude: number;
    maxSpaces: number;
    name: string;
    occupiedSpaces: number;
    payingDevices: PayingDevice[];
    sensors: Sensor[];
    zone: Zone;
    zoneId: number;
}
