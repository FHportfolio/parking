export class Parking {
    public enabled: boolean;
    public id: string;
    public latitude: string;
    public longitude: string;
    public maxSpaces: number;
    public name: string;
    public occupiedSpaces: number;
    public zoneId: number;
    
    constructor(enabled: boolean, id: string, latitude: string,
        longitude: string, maxSpaces: number, name: string, occupiedSpaces: number, zoneId: number){
        this.enabled = enabled;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.maxSpaces = maxSpaces;
        this.name = name;
        this.occupiedSpaces = occupiedSpaces;
        this.zoneId = zoneId; 
    }
}