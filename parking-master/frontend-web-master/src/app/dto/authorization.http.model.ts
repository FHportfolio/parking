import { UserA } from "./user";

export class AuthorizationData{
    access_token: string;
    expires_in: any;
    jti: string;
    refresh_token: string; 
    scope: string;
    token_type: string;
    user_data: UserA;
}