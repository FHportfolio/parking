import { Component, OnInit, OnDestroy } from '@angular/core';
import { Sensor } from '../dto/sensor.model';
import { ActivatedRoute, Params } from '@angular/router';
import { ParkingService } from '../services/parking.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { DialogSensor } from '../dialog-sensor/dialog-sensor.component';

declare var ol: any;

@Component({
  selector: 'app-parking-details',
  templateUrl: './parking-details.component.html',
  styleUrls: ['./parking-details.component.css']
})
export class ParkingDetailsComponent implements OnInit, OnDestroy {

  id: string;

  sensors: Sensor[] = [];

  subscription: Subscription;

  latitude: number = 18.5204;
  longitude: number = 73.8567;

  /* ol: any; */
  map: any;

  /* displayedColumns: string[] = ['id', 'lastUpdated', 'latitude', 'longitude', 'occupied', 'parkingId', 'symbol']; */

  /* dataSource = this.sensors; */

  constructor(private route: ActivatedRoute, private parkingService: ParkingService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.subscription = this.parkingService.sensorsChanged
      .subscribe(
        (sensors: Sensor[]) => {
          this.sensors = sensors;
        }
      );
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params['id'];
        console.log(this.id);
        /* this.dataSource = this.sensors.filter((sensor) => sensor.parkingId === this.id); */
      }
    );
    this.parkingService.getsensorsFromBackend(this.id).subscribe({
      next: (sensors) => {
        console.log(sensors.sensors);
        this.parkingService.setSensors(sensors.sensors);
      },
      error: (error) => console.log(error)
    });

    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([15.972313, 45.800894]),
        zoom: 13
      })
    });
  
    this.map.on('click', function (args) {
      console.log(args.coordinate);
      var lonlat = ol.proj.transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
      console.log(lonlat);
      
      var lon = lonlat[0];
      var lat = lonlat[1];
      alert(`Latitude: ${lat} Longitude: ${lon}`);
    });

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onClick(i: number) {
    /* console.log(this.parkings[i]); */
    const dialogRef = this.dialog.open(DialogSensor, {
      width: '250px',
      data: { sensor: this.sensors[i] }
    });

    dialogRef.afterClosed().subscribe(result => {
      /*       console.log(`Dialog result: ${result}`);
       */      /* this.parking = result; */
      if (!(result === undefined )) {
        console.log(result);
        this.parkingService.updateSensor(i, result).subscribe(
          (response) => {
            console.log(response);
          }
        );
      }
      else{
        console.log("Greska pri dodavanju!");
      }
      /* this.parking = new Parking(null,null,null,null,null,null); */
    });
  }

  onDelete(i) {
    this.parkingService.deleteSensor(i, this.sensors[i]).subscribe(
      (response) => {
        console.log(response);
      }
    );
  }


}
