import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingZoneComponent } from './existing-zone.component';

describe('ExistingZoneComponent', () => {
  let component: ExistingZoneComponent;
  let fixture: ComponentFixture<ExistingZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
