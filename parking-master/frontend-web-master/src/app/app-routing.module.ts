import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ParkingDetailsComponent } from './parking-details/parking-details.component';
import { ModManageComponent } from './mod-manage/mod-manage.component';
import { SigninComponent } from './signin/signin.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { singnInRoutes } from './signin/signin-routing-module';
import { ParkingZonesComponent } from './parking-zones/parking-zones.component';
import { ZONES_ROUTES } from './parking-zones/parking-zones.routes';
import { AuthGuard } from './services/auth-guard.setvice';
import { Role } from './dto/role';
import { LoginComponent } from './login/login.component';
import { PayingDevicesComponent } from './paying-devices/paying-devices.component';

const routes: Routes = [
  {path: '', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'details/:id', component: ParkingDetailsComponent, canActivate: [AuthGuard]},
  {path: 'paying/:id', component: PayingDevicesComponent, canActivate: [AuthGuard]},

  {path: 'moderator',
   component: ModManageComponent, 
   canActivate: [AuthGuard], 
   data: { roles: [Role.Admin]}
  },

  {path: 'signin', component: SigninComponent, children: singnInRoutes},
  {path: 'editProfile/:id', component: EditProfileComponent},
  {path: 'zones', component: ParkingZonesComponent, children: ZONES_ROUTES, canActivate: [AuthGuard]},
  { 
    path: 'login', 
    component: LoginComponent 
},

// otherwise redirect to home
{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
