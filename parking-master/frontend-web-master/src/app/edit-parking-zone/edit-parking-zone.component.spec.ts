import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditParkingZoneComponent } from './edit-parking-zone.component';

describe('EditParkingZoneComponent', () => {
  let component: EditParkingZoneComponent;
  let fixture: ComponentFixture<EditParkingZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditParkingZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditParkingZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
