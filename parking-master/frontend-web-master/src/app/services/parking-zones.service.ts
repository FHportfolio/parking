import { Injectable } from '@angular/core';
import { Zone } from '../parking-zones/zone';
import { EventEmitter } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParkingZonesService {
  zones: Zone[]=[];
  zonesChanged = new Subject<Zone[]>();

  constructor(private httpClient: HttpClient) { }

  

  getZones(){
    return this.zones.slice();
  }

  setZones(zones: Zone[]) {
    this.zones = zones;
    this.zonesChanged.next(this.zones.slice());
}

/*  */
  getZone(index: number){
    return this.zones[index];
  }
/*  */

getZonesFromBackend() {
  return this.httpClient.get<Zone[]>('/parking/admin/zone/');
}

addZone(zone: Zone) {
  this.zones.push(zone);
  this.zonesChanged.next(this.zones.slice());
  
  const data = {
    "amount": zone.amount,
    "currency": zone.currency,
    "name": zone.name,
    "phone": zone.phone
  };

  const req = new HttpRequest('POST', '/parking/admin/zone/register', data , { reportProgress: true })
  return this.httpClient.request(req);
}

updateZone(index: number, newZone: Zone) {
  this.zones[index] = newZone;
  this.zonesChanged.next(this.zones.slice());
  const data = {
    "id": newZone.id,
    "amount": newZone.amount,
    "currency": newZone.currency,
    "name": newZone.name,
    "phone": newZone.phone
  };

  const req = new HttpRequest('PUT', '/parking/admin/zone/edit', data , { reportProgress: true })
  return this.httpClient.request(req);
}

deleteZone(index: number, zone: Zone) {
  this.zones.splice(index, 1);
  this.zonesChanged.next(this.zones.slice());
  
  const req = new HttpRequest('DELETE', `/parking/admin/zone/delete?id=${zone.id}`, { reportProgress: true }); 
  return this.httpClient.request(req);
}

}
