import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UserA } from '../dto/user';

@Injectable({ providedIn: 'root' })
export class UserAService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<UserA[]>(`/authorization/user/pageable`);
    }

    /* getById(id: number) {
        return this.http.get<UserA>(`${config.apiUrl}/users/${id}`);
    } */
}