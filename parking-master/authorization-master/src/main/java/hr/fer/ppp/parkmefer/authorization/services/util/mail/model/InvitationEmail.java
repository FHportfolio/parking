package hr.fer.ppp.parkmefer.authorization.services.util.mail.model;

import hr.fer.ppp.parkmefer.authorization.services.util.mail.MailContentAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class InvitationEmail extends MailContentAdapter {

    @Value("classpath:/static/html/invitation.html")
    private Resource templateResource;


    public InvitationEmail() {
    }

    @PostConstruct
    private void init() {
        try {
            super.setResource(templateResource.getInputStream());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }
}
