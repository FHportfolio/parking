package hr.fer.ppp.parkmefer.authorization.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

@EnableResourceServer
@Configuration
public class ResourceServer extends ResourceServerConfigurerAdapter {

    private static final String[] ENDPOINTS = {
            "/register",
            "/swagger-ui.html",
            "/swagger-resources/**",
            "/error",
            "/",
            "/csrf",
            "/v2/api-docs",
            "/css/**",
            "/js/**",
            "/img/**",
            "/**/favicon.ico",
            "/webjars/**"};
    private static final String[] ADMIN_ONLY_ENDPOINTS = {"/user/invite", "/user/delete", "/user/pageable", "/user/email"};
    private static final String ADMIN = "Admin";

    private final DefaultTokenServices tokenServices;

    @Autowired
    public ResourceServer(DefaultTokenServices tokenServices) {
        this.tokenServices = tokenServices;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().sameOrigin();

        http.
                authorizeRequests()
                .antMatchers(ENDPOINTS).permitAll()
                .and().authorizeRequests().antMatchers(ADMIN_ONLY_ENDPOINTS).hasAuthority(ADMIN)
                .and().authorizeRequests().anyRequest().authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenServices);
    }
}
