package hr.fer.ppp.parkmefer.authorization.dto.mappers.impl;

import hr.fer.ppp.parkmefer.authorization.dto.UserDTO;
import hr.fer.ppp.parkmefer.authorization.dto.mappers.Mapper;
import hr.fer.ppp.parkmefer.authorization.entities.User;
import hr.fer.ppp.parkmefer.authorization.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapper<User, UserDTO> {

    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    @Autowired
    public UserMapper(@Lazy PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }


    @Override
    public User dtoToEntity(UserDTO dto) {
        User entity = new User();

        entity.setName(dto.getName());
        entity.setSurname(dto.getSurname());
        entity.setEmail(dto.getEmail());
        entity.setPasswordHash(passwordEncoder.encode(dto.getPassword()));
        entity.setRole(roleRepository.findById(dto.getRoleId()).orElse(null));
        entity.setPhone(dto.getPhone());

        return entity;
    }

    @Override
    public UserDTO entityToDto(User entity) {
        UserDTO dto = new UserDTO();

        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setEmail(entity.getEmail());
        dto.setID(entity.getID());
        dto.setPhone(entity.getPhone());

        return dto;
    }
}
