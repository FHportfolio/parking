package hr.fer.ppp.parkmefer.authorization.controllers;

import hr.fer.ppp.parkmefer.authorization.dto.InviteDTO;
import hr.fer.ppp.parkmefer.authorization.dto.ResponseDTO;
import hr.fer.ppp.parkmefer.authorization.dto.UserDTO;
import hr.fer.ppp.parkmefer.authorization.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(path = "user")
public class UserController extends AbstractController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService, MessageSource messageSource) {
        super(messageSource);

        this.userService = userService;
    }

    @GetMapping(path = "/email", produces = "application/json")
    public ResponseEntity<UserDTO> getUserByEmail(@RequestParam String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

    @PostMapping(path = "/invite", produces = "application/json")
    public ResponseEntity<ResponseDTO> inviteNewUser(@RequestBody @Valid InviteDTO invite, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().body(createResponseDTO(result));
        }

        if (userService.inviteNewUser(invite)) {
            return ResponseEntity.ok(createResponseDTO());
        } else {
            ResponseDTO responseDTO = createResponseDTO();
            responseDTO.getErrors().put("error", "Duplicate invite!");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(responseDTO);
        }
    }

    @GetMapping(path = "/pageable", produces = "application/json")
    public ResponseEntity<Page<UserDTO>> getUsersPageable(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
        return ResponseEntity.ok(userService.getUsersPageable(PageRequest.of(page, size)));
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteUser(@RequestParam UUID id) {
        userService.deleteUserById(id);
        return ResponseEntity.ok(createResponseDTO());
    }
}