package hr.fer.ppp.parkmefer.authorization.services.util;

import hr.fer.ppp.parkmefer.authorization.common.files.CustomTextFileReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class TermsOfServiceLoader {

    @Value("classpath:/legal/terms-of-service")
    private Resource termsOfServiceResource;
    private String termsOfService;

    @PostConstruct
    public void init() {
        try {
            loadFromFile();
        } catch (IOException ex) {
            throw new RuntimeException("Terms of service file could not be found. ", ex);
        }
    }

    private void loadFromFile() throws IOException {
        this.termsOfService = CustomTextFileReader.readFromInputStream(termsOfServiceResource.getInputStream());
    }

    public String getTermsOfService() {
        return termsOfService;
    }
}
