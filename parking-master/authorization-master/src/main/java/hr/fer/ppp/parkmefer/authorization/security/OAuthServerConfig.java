package hr.fer.ppp.parkmefer.authorization.security;

import hr.fer.ppp.parkmefer.authorization.common.files.CustomTextFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.io.IOException;
import java.util.Arrays;

@Configuration
@EnableAuthorizationServer
public class OAuthServerConfig extends AuthorizationServerConfigurerAdapter {

    private static final String PERMISSION_LEVEL = "permitAll()";
    private static final String CLIENT_ID = "user";
    private static final String CLIENT_SECRET = "user";
    private static final String[] GRANT_TYPES = {"client_credentials", "password", "refresh_token"};
    private static final String[] SCOPES = {"read", "write", "trust", "all"};
    private static final String[] AUTHORITIES = {"Admin", "Mod"};

    private final AuthenticationManager authenticationManager;
    private final TokenEnhancer tokenEnhancer;

    @Value("classpath:/security/park-me-fer")
    private Resource privateKey;

    @Value("classpath:/security/park-me-fer.pub")
    private Resource publicKey;

    @Autowired
    public OAuthServerConfig(AuthenticationManager authenticationManager, TokenEnhancer tokenEnhancer) {
        this.authenticationManager = authenticationManager;
        this.tokenEnhancer = tokenEnhancer;
    }

    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess(PERMISSION_LEVEL).checkTokenAccess(PERMISSION_LEVEL).addTokenEndpointAuthenticationFilter(new CorsFilter());
    }

    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(CLIENT_ID)
                .secret(passwordEncoder().encode(CLIENT_SECRET))
                .authorizedGrantTypes(GRANT_TYPES).scopes(SCOPES)
                .accessTokenValiditySeconds(60 * 60 * 12) //12 hours
                .refreshTokenValiditySeconds(60 * 60 * 24 * 30); //30 days
    }

    @Override
    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer, accessTokenConverter()));
        endpoints.tokenStore(tokenStore()).tokenEnhancer(tokenEnhancerChain).authenticationManager(authenticationManager);
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        UserAuthenticationConverter userAuthenticationConverter = new AuthToPrincipal();

        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(userAuthenticationConverter);
        accessTokenConverter.setIncludeGrantType(true);

        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setAccessTokenConverter(accessTokenConverter);

        try {
            String privateKeyAsString = CustomTextFileReader.readFromInputStream(privateKey.getInputStream());
            String publicKeyAsString = CustomTextFileReader.readFromInputStream(publicKey.getInputStream());

            converter.setSigningKey(privateKeyAsString);
            converter.setVerifierKey(publicKeyAsString);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        return converter;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
