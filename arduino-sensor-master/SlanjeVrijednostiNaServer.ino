#include <ArduinoJson.h>
#include <BridgeClient.h>
#include <Bridge.h>
#include <Process.h>
#include <HttpClient.h>



BridgeClient client;


//Konstante koje odreduju pinove na koje se spaja ultrazvucni senzor
const int trigPin = 10; //Input pin
const int echoPin = 11; //Output pin
const int led = 13;
//Interval nakon kojeg se osvjezava rezultat za parkirno mjesto 
const long interval =  300;

const int udaljenostSlobodno = 200;

//izmjerena udaljenost je u cm ,a trajanje putovanja zvuka u uS


//byte ip[] = { 10, 0, 0, 177 }; 
byte server[] = { 192,168,5,43 }; //ip address 

void setup() {

  Serial.begin(9600);

  Bridge.begin();
  pinMode(led,OUTPUT);
  pinMode(trigPin, OUTPUT); 
  pinMode(echoPin, INPUT);  
  
}

void loop() {

 
  
  Serial.println("Start");
  Process wifiCheck;
  wifiCheck.runShellCommand("/usr/bin/pretty-wifi-info.lua"); 
  while (wifiCheck.available() > 0) {
    char c = wifiCheck.read();
    Serial.print(c);
  }

  Serial.println();

  delay(1000);

  long trajanjePutovanjaZvuka = 0;
  long izmjerenaUdaljenost = 0;

// Resetiranje triggera 
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
// Kako bi se emitirao zvuk potrebno je postaviti trigger na 1 na 10uS
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW);
  

// Vraca vrijeme povratka zvuka u uS
  trajanjePutovanjaZvuka = pulseIn(echoPin, HIGH);
 
  izmjerenaUdaljenost = trajanjePutovanjaZvuka*0.034/2;
  Serial.println(izmjerenaUdaljenost);
  Serial.println(trajanjePutovanjaZvuka);




 //Povezivanje sa serverom
  Serial.println("connectng");
  if (client.connect(server, 8082)) {
    Serial.println("connected");
    //client.println("GET /search?q=arduino HTTP/1.0");
    //client.println();
  } else {
    Serial.println("connection failed");
  }

  delay(1000);

  
/*
  const size_t bufferSize = JSON_OBJECT_SIZE(2);
    DynamicJsonBuffer jsonBuffer(bufferSize);

    JsonObject& root = jsonBuffer.createObject();
    root["sensorId"] = "2bb69b94-0e0e-4820-8afb-c6cfd8f0a4ac";
    root["occupied"] = true;
  */  


  //Slanje na server
 Process p;
  if(izmjerenaUdaljenost < udaljenostSlobodno)   
  {
    p.runShellCommand("curl -X POST -d '{\"sensorId\": \"2bb69b94-0e0e-4820-8afb-c6cfd8f0a4ac\", \"occupied\": \"true\"}' -H \"Content-Type: application/json\" 192.168.5.43:8082/io/");


  //slanje informacije da je mjesto zauzeto na server
    
  }else{
    p.runShellCommand("curl -X POST -d '{\"sensorId\": \"2bb69b94-0e0e-4820-8afb-c6cfd8f0a4ac\", \"occupied\": \"false\"}' -H \"Content-Type: application/json\" 192.168.5.43:8082/io/");
    
  }

}

    delay(1000);
  

}
